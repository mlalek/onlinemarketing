const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const wishlistSchema = new Schema({
  username: { type: String, required: true },
  itemid: { type: String, required: true },
  date: { type: Date, required: true },
},
{
  timestamps: true,
});

const Wishlist = mongoose.model('Wishlist', wishlistSchema);

module.exports = Wishlist;