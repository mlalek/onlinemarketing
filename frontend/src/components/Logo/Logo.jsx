import React from 'react';
import burgerLogo from '../../assets/Images/second-hand2.svg';
import classes from './Logo.module.css';

const Logo=props=>(
<div className={classes.Logo} style={{height:props.height}}>
    <img src={burgerLogo} alt="MyBurger"/>
</div>
);

export default Logo;