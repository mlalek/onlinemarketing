import React, { useState, useEffect } from "react";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import Spinner from "./components/UI/Spinner/Spinner";
import Layout from "./hoc/Layout/Layout";
import LoginPage from './pages/login';
import RegisterPage from './pages/register';
import Index from "./pages/index";
import CreateItem from "./pages/create-item";


function App(props) {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  useEffect(() => {
    const dummy = process.env.REACT_APP_DUMMY_USER;
    !!dummy && setIsAuthenticated(true);
  })
  let routes = (
    <Switch>
      <Route path="/" exact component={Index}/>
      <Route path="/create-item" exact component={CreateItem}/>
      <Route path="/register" exact component={RegisterPage}/>

    </Switch>
  );

  if (isAuthenticated) {
    routes = (
      <Switch>
        <Route path="/checkout" />
        <Route path="/orders" />
        <Route path="/logout" />
        <Route path="/" exact />
      
        <Redirect to="/" />
      </Switch>
    );
  } else {
    
  }
  return (
    <Layout>
      {/* <Spinner /> */}
      <div className="container">
      {routes}      
      </div>
     
    </Layout>
  );
}

export default withRouter(App);
